package main

import "time"

type LAN struct {
	ID           string
	Creator      string
	Date         time.Time
	Location     string
	Games        []string
	Participants []string
}
