package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func main() {
	fmt.Println("Hello Word")
	r := gin.Default()

	r.GET("/lans", listLANs)
	r.POST("/lans", createLAN)
	r.PUT("/lans/:id", editLAN)
	r.GET("/lans/:id", getLAN)
	r.DELETE("/lans/:id", deleteLAN)

	r.Run()

}
