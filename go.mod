module gitlab.com/DarkKem/api

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
)
