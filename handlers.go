package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

var lans []LAN = []LAN{}

func listLANs(c *gin.Context) {
	c.JSON(http.StatusOK, lans)
}
func createLAN(c *gin.Context) {
	var newLan LAN
	if err := c.ShouldBindJSON(&newLan); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error()})
	}

	newLan.ID = uuid.NewString()

	lans = append(lans, newLan)

	c.JSON(http.StatusCreated, newLan)

}
func editLAN(c *gin.Context) {
	id := c.Param("id")

	for lan := range lans {
		if lans[lan].ID == id {
			var newLan LAN
			if err := c.ShouldBindJSON(&newLan); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
			lans[lan].Date = newLan.Date
			lans[lan].Creator = newLan.Creator
			lans[lan].Games = newLan.Games
			lans[lan].Participants = newLan.Participants
			lans[lan].Location = newLan.Location

			c.JSON(http.StatusOK, lans[lan])
			return
		}
	}

	c.JSON(http.StatusNotFound, gin.H{
		"message": "LAN does not exist",
	})
}

func getLAN(c *gin.Context) {
	id := c.Param("id")

	for lan := range lans {
		if lans[lan].ID == id {
			c.JSON(http.StatusOK, lans[lan])
			return
		}
	}
	c.JSON(http.StatusNotFound, gin.H{
		"message": "LAN does not exist",
	})
}

func deleteLAN(c *gin.Context) {
	id := c.Param("id")

	lanToDelete := -1
	for lan := range lans {
		if lans[lan].ID == id {
			lanToDelete = lan
			break
		}
	}
	if lanToDelete == -1 {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "Lan not found",
		})
	}
	lans = append(lans[:lanToDelete], lans[lanToDelete+1:]...)

	c.JSON(http.StatusOK, gin.H{
		"message": "Lan deleted",
	})
}
